package com.company;

import com.company.domain.Employee;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private List<Employee> employees;

    public Company() {
        employees = new ArrayList();
    }

    public int costOfWork() {
        int res = this.employees.stream().mapToInt(Employee::getSalary).sum();
        return res;
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }
}

package com.company.domain;

public interface WorkableFrontendDeveloper extends WorkableDeveloper {
    void writeJavaScript ();

    @Override
    void develop();

    @Override
    void work();
}

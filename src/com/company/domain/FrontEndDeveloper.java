package com.company.domain;

public final class FrontEndDeveloper extends Developer implements WorkableFrontendDeveloper{
    public FrontEndDeveloper (String name, int salary) {
        super(name, salary);
    }

    @Override
    public void develop() {
        super.develop();
    }

    @Override
    public void work() {
        super.work();
    }

    @Override
    public void writeJavaScript() {
        System.out.println("I'm writing in JS");
    }
}
package com.company.domain;

public interface WorkableBackendDeveloper extends WorkableDeveloper {
    void writeJavaCode();

    @Override
    void develop();

    @Override
    void work();
}

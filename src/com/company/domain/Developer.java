package com.company.domain;

public abstract class Developer extends Employee implements WorkableDeveloper {

    public Developer(String name, int salary) {
        super(name, salary);
    }

    public void develop() {
        System.out.println("I'm developing something");
    }

    @Override
    public void work() {
        super.work();
    }
}

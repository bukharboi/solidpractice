package com.company.domain;

public interface WorkableDeveloper extends WorkableEmployee {
    void develop ();
    @Override
    void work();
}

package com.company.domain;

public abstract class Employee implements WorkableEmployee {
    private int empID;
    private static int id_gen = 0;
    private String name;
    private int salary;

    public Employee(String name, int salary) {
        empID = ++id_gen;
        this.name = name;
        this.salary = salary;
    }

    public int getEmpID() {
        return empID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "[" + empID + " - " + name + " - " + salary + "]";
    }

    @Override
    public void work() {
        System.out.println("I'm working");
    }
}
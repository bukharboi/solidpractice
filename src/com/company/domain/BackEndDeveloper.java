package com.company.domain;

public final class BackEndDeveloper extends Developer implements WorkableBackendDeveloper {
    public BackEndDeveloper(String name, int salary) {
        super(name, salary);
    }

    @Override
    public void develop() {
        super.develop();
    }

    @Override
    public void work() {
        super.work();
    }

    @Override
    public void writeJavaCode() {
        System.out.println("I'm writing in Java");
    }
}
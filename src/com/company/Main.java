package com.company;

import com.company.domain.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\db.txt");
        Scanner sc = new Scanner(file);
        Company company = new Company();
        while (sc.hasNextInt()) {
            int type = sc.nextInt();
            String name = sc.next();
            int salary = sc.nextInt();

            if (type == 1) {
                company.addEmployee(new BackEndDeveloper(name, salary));
            } else {
                company.addEmployee(new FrontEndDeveloper(name, salary));
            }
        }
        System.out.println(company.costOfWork());
    }
}